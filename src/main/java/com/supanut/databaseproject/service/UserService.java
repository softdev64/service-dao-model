package com.supanut.databaseproject.service;

import com.supanut.databaseproject.dao.UserDao;
import com.supanut.databaseproject.model.User;

public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
}
