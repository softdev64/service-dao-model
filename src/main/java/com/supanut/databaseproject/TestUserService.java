package com.supanut.databaseproject;

import com.supanut.databaseproject.dao.UserDao;
import com.supanut.databaseproject.model.User;
import com.supanut.databaseproject.service.UserService;

public class TestUserService {

    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user2", "password1");
        if (user != null) {
            System.out.println("Welcome user : " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}
